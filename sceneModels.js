function emptyModelFeatures() {

    // EMPTY MODEL

    this.vertices = [];

    this.colors = [];

    this.normals = [];

    // Transformation parameters

    // Displacement vector

    this.tx = 0.0;

    this.ty = 0.0;

    this.tz = 0.0;

    // Rotation angles

    this.rotAngleXX = 0.0;

    this.rotAngleYY = 0.0;

    this.rotAngleZZ = 0.0;

    // Scaling factors

    this.sx = 1.0;

    this.sy = 1.0;

    this.sz = 1.0;

    // Animation controls

    this.rotXXOn = false;

    this.rotYYOn = false;

    this.rotZZOn = false;

    this.rotXXSpeed = 1.0;

    this.rotYYSpeed = 1.0;

    this.rotZZSpeed = 1.0;

    this.rotXXDir = 1;

    this.rotYYDir = 1;

    this.rotZZDir = 1;

    // ADDED

    this.map = 0;
    this.building = 0;

    // Material features

    this.kAmbi = [0.2, 0.2, 0.2];

    this.kDiff = [0.7, 0.7, 0.7];

    this.kSpec = [0.7, 0.7, 0.7];

    this.nPhong = 100;
}

function tree() {
    var tree = new emptyModelFeatures();

    tree.kAmbi = [0.19, 0.07, 0.02];
    tree.kDiff = [0.7, 0.27, 0.08];
    tree.kSpec = [0.26, 0.14, 0.08];
    tree.nPhong = 12.8;

    tree.vertices = [
        // Tronco
        0.0, 1.0, 0.0,
        1.0, 1.0, 0.0,
        0.5, 1.0, -0.866,
        0.0, 1.0, 0.0,
        0.5, 1.0, -0.866,
        -0.5, 1.0, -0.866,
        0.0, 1.0, 0.0,
        -0.5, 1.0, -0.866,
        -1.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
        -1.0, 1.0, 0.0,
        -0.5, 1.0, 0.866,
        0.0, 1.0, 0.0,
        -0.5, 1.0, 0.866,
        0.5, 1.0, 0.866,
        0.0, 1.0, 0.0,
        0.5, 1.0, 0.866,
        1.0, 1.0, 0.0,
        0.0, -1.0, 0.0,
        0.5, -1.0, -0.866,
        1.0, -1.0, 0.0,
        0.0, -1.0, 0.0,
        -0.5, -1.0, -0.866,
        0.5, -1.0, -0.866,
        0.0, -1.0, 0.0,
        -1.0, -1.0, 0.0,
        -0.5, -1.0, -0.866,
        0.0, -1.0, 0.0,
        -0.5, -1.0, 0.866,
        -1.0, -1.0, 0.0,
        0.0, -1.0, 0.0,
        0.5, -1.0, 0.866,
        -0.5, -1.0, 0.866,
        0.0, -1.0, 0.0,
        1.0, -1.0, 0.0,
        0.5, -1.0, 0.866,
        -0.5, 1.0, 0.866,
        0.5, -1.0, 0.866,
        0.5, 1.0, 0.866,
        -0.5, 1.0, 0.866,
        -0.5, -1.0, 0.866,
        0.5, -1.0, 0.866,
        0.5, 1.0, 0.866,
        0.5, -1.0, 0.866,
        1.0, -1.0, 0.0,
        0.5, 1.0, 0.866,
        1.0, -1.0, 0.0,
        1.0, 1.0, 0.0,
        1.0, -1.0, 0.0,
        0.5, 1.0, -0.866,
        1.0, 1.0, 0.0,
        0.5, -1.0, -0.866,
        0.5, 1.0, -0.866,
        1.0, -1.0, 0.0,
        0.5, -1.0, -0.866,
        -0.5, 1.0, -0.866,
        0.5, 1.0, -0.866,
        0.5, -1.0, -0.866,
        -0.5, -1.0, -0.866,
        -0.5, 1.0, -0.866,
        -0.5, -1.0, -0.866,
        -1.0, 1.0, 0.0,
        -0.5, 1.0, -0.866,
        -0.5, -1.0, -0.866,
        -1.0, -1.0, 0.0,
        -1.0, 1.0, 0.0,
        -0.5, -1.0, -0.866,
        -1.0, 1.0, 0.0,
        -0.5, 1.0, -0.866,
        -0.5, -1.0, -0.866,
        -1.0, -1.0, 0.0,
        -1.0, 1.0, 0.0,
        -1.0, -1.0, 0.0,
        -0.5, -1.0, 0.866,
        -1.0, 1.0, 0.0,
        -0.5, -1.0, 0.866,
        -0.5, 1.0, 0.866,
        -1.0, 1.0, 0.0,
    ];

    tree.colors = tree.vertices.slice()

    for (var i = 0; i < tree.colors.length; i++) {
        if (i % 3 == 0) {
            tree.colors[i] = 1.0;
            tree.colors[i + 1] = 0.0;
            tree.colors[i + 2] = 0.0;
        }
    }

    computeVertexNormals(tree.vertices, tree.normals);

    tree.map = 1;

    return tree;
}

function sphereModel(subdivisionDepth = 2) {

    var sphere = new emptyModelFeatures();

    sphere.vertices = [
		-1.000000, -1.000000,  1.000000,
		 1.000000,  1.000000,  1.000000,
		-1.000000,  1.000000,  1.000000,
		-1.000000, -1.000000,  1.000000,
		 1.000000, -1.000000,  1.000000,
		 1.000000,  1.000000,  1.000000,
         1.000000, -1.000000,  1.000000,
		 1.000000, -1.000000, -1.000000,
		 1.000000,  1.000000, -1.000000,
         1.000000, -1.000000,  1.000000,
         1.000000,  1.000000, -1.000000,
         1.000000,  1.000000,  1.000000,
        -1.000000, -1.000000, -1.000000,
        -1.000000,  1.000000, -1.000000,
         1.000000,  1.000000, -1.000000,
        -1.000000, -1.000000, -1.000000,
         1.000000,  1.000000, -1.000000,
         1.000000, -1.000000, -1.000000,
        -1.000000, -1.000000, -1.000000,
		-1.000000, -1.000000,  1.000000,
		-1.000000,  1.000000, -1.000000,
		-1.000000, -1.000000,  1.000000,
		-1.000000,  1.000000,  1.000000,
		-1.000000,  1.000000, -1.000000,
		-1.000000,  1.000000, -1.000000,
		-1.000000,  1.000000,  1.000000,
		 1.000000,  1.000000, -1.000000,
		-1.000000,  1.000000,  1.000000,
		 1.000000,  1.000000,  1.000000,
		 1.000000,  1.000000, -1.000000,
		-1.000000, -1.000000,  1.000000,
		-1.000000, -1.000000, -1.000000,
		 1.000000, -1.000000, -1.000000,
		-1.000000, -1.000000,  1.000000,
		 1.000000, -1.000000, -1.000000,
		 1.000000, -1.000000,  1.000000,
    ];

    midPointRefinement(sphere.vertices, sphere.colors, subdivisionDepth);

    moveToSphericalSurface(sphere.vertices)

    computeVertexNormals(sphere.vertices, sphere.normals);

    return sphere;
}

function simpleCubeModel( ) {
    var cube = new emptyModelFeatures();

    cube.kAmbi = [0.05, 0.05, 0.05];
    cube.kDiff = [0.5, 0.5, 0.5,];
    cube.kSpec = [0.7, 0.7, 0.7];

	cube.vertices = [
		-1.000000, -1.000000,  1.000000,
		 1.000000,  1.000000,  1.000000,
		-1.000000,  1.000000,  1.000000,
		-1.000000, -1.000000,  1.000000,
		 1.000000, -1.000000,  1.000000,
		 1.000000,  1.000000,  1.000000,
         1.000000, -1.000000,  1.000000,
		 1.000000, -1.000000, -1.000000,
		 1.000000,  1.000000, -1.000000,
         1.000000, -1.000000,  1.000000,
         1.000000,  1.000000, -1.000000,
         1.000000,  1.000000,  1.000000,
        -1.000000, -1.000000, -1.000000,
        -1.000000,  1.000000, -1.000000,
         1.000000,  1.000000, -1.000000,
        -1.000000, -1.000000, -1.000000,
         1.000000,  1.000000, -1.000000,
         1.000000, -1.000000, -1.000000,
        -1.000000, -1.000000, -1.000000,
		-1.000000, -1.000000,  1.000000,
		-1.000000,  1.000000, -1.000000,
		-1.000000, -1.000000,  1.000000,
		-1.000000,  1.000000,  1.000000,
		-1.000000,  1.000000, -1.000000,
		-1.000000,  1.000000, -1.000000,
		-1.000000,  1.000000,  1.000000,
		 1.000000,  1.000000, -1.000000,
		-1.000000,  1.000000,  1.000000,
		 1.000000,  1.000000,  1.000000,
		 1.000000,  1.000000, -1.000000,
		-1.000000, -1.000000,  1.000000,
		-1.000000, -1.000000, -1.000000,
		 1.000000, -1.000000, -1.000000,
		-1.000000, -1.000000,  1.000000,
		 1.000000, -1.000000, -1.000000,
		 1.000000, -1.000000,  1.000000,
    ];

    cube.colors = cube.vertices.slice()

    for (var i = 0; i < cube.colors.length; i++) {
        if (i % 3 == 0) {
            cube.colors[i] = 0.4;
            cube.colors[i+1] = 0.4;
            cube.colors[i+2] = 0.4;
        }
    }

    computeVertexNormals(cube.vertices, cube.normals);

    cube.map = 1;
    cube.building = 1;

	return cube;
}

function floor(x_in=100, y_in = -0.25, z_in=100 , colors=[0.3, 1.0, 0.4], type="mountain", mesh=true, properties=[
    [0.0, 0.0, 0.0],
    [0.1, 0.35, 0.1],
    [0.45, 0.55, 0.45]
], r=5) {
    var floor = new emptyModelFeatures();
    //console.log(y)

    floor.kAmbi = properties[0];
    floor.kDiff = properties[1];
    floor.kSpec = properties[2];

    floor.vertices = [
        -x_in, y_in, -z_in,
        -x_in, y_in,  z_in,
         x_in, y_in, -z_in,
         x_in, y_in, -z_in,
        -x_in, y_in,  z_in,
         x_in, y_in,  z_in
    ];

    floor.colors = floor.vertices.slice()

    for (var i = 0; i < floor.colors.length; i++) {
        if (i % 3 == 0) {
            floor.colors[i] = colors[0];
            floor.colors[i+1] = colors[1];
            floor.colors[i+2] = colors[2];
        }
    }

    floor.map = 1;

    if (mesh) {
        midPointRefinement(floor.vertices, floor.colors, r);
    }


    if (type == "mountain") {
        for (var i = 0; i < floor.vertices.length; i++) {
            if (i % 3 == 0) {
                x = floor.vertices[i];
                y = floor.vertices[i + 1];
                z = floor.vertices[i + 2];
                //var c = -0.002 * x**2 + 5;
                //Equação: (0.01 * x**2) + (0.01 * y**2) + (10*z) = -50
                var c = (((-0.01 * x**2) - (0.01 * z**2) + 50) / 10);
                temp = c + (Math.random() * 2) + y_in;

                //console.log("FLOOR", x, y, z);
                // Checks the equal ones
                for (var j = 0; j < floor.vertices.length; j++) {
                    if (j % 3 == 0) {
                        if (x == floor.vertices[j] && y == floor.vertices[j + 1] && z == floor.vertices[j + 2]) {
                            if (x >= -71 && x <= 71) {
                                floor.vertices[j+1] = temp;
                            }
                        }
                    }
                }
            }
        }
    }
    if (type == "beach") {
        for (var i = 0; i < floor.vertices.length; i++) {
            if (i % 3 == 0) {
                x = floor.vertices[i];
                y = floor.vertices[i + 1];
                z = floor.vertices[i + 2];
                //var c = -0.002 * x**2 + 5;
                //Equação: (-0.01 * x**2) - (0.01 * y**2) - (4.1*z) = -95
                var c = (((-0.01 * x**2) - (0.01 * z**2) + 50) / 100);
                temp = c + (Math.random() * 2) + y_in;

                //console.log("FLOOR", x, y, z);
                // Checks the equal ones
                for (var j = 0; j < floor.vertices.length; j++) {
                    if (j % 3 == 0) {
                        if (x == floor.vertices[j] && y == floor.vertices[j + 1] && z == floor.vertices[j + 2]) {
                            if (x >= -71 && x <= 71) {
                                floor.vertices[j+1] = temp;
                            }
                        }
                    }
                }
            }
        }
    }
    computeVertexNormals(floor.vertices, floor.normals);

    return floor;
}

function simplePaddlesModel() {

    var paddle = new emptyModelFeatures();

    paddle.vertices = [
        -0.05, 0.0, -0.5,
         0.05, 0.0, -0.5,
         0.05, 0.0, 0.5,

         0.05, 0.0, 0.5,
        -0.05, 0.0, 0.5,
        -0.05, 0.0, -0.5,

         0.5, 0.0, 0.05,
        -0.5, 0.0, -0.05,
         0.5, 0.0, -0.05,

         0.5, 0.0, 0.05,
        -0.5, 0.0, 0.05,
        -0.5, 0.0, -0.05
    ];

    paddle.colors = paddle.vertices.slice()

    for (var i = 0; i < paddle.colors.length; i++) {
        if (i % 3 == 0) {
            paddle.colors[i] = 0;
            paddle.colors[i+1] = 0;
            paddle.colors[i+2] = 0;
        }
    }

    computeVertexNormals(paddle.vertices, paddle.normals);

    paddle.map = 0;

    return paddle;

}

var sceneModels = [];

// Oceano
sceneModels.push(new floor(200, -0.1, 200, [0, 0, 0.9], "sea", true, [
    [0.1, 0.18725, 0.1745],
    [0.396, 0.74151, 0.69102],
    [0.297254, 0.30829, 0.306678]
], 7))

// Chão
sceneModels.push(new floor(100, -0.5, 100, [0.3, 1.0, 0.4], "mountain", true))

// Areia
sceneModels.push(new floor(110, -1, 110, [1.0, 1.0, 0.0], "beach", true, [
    [0.05, 0.05, 0.0],
    [0.5, 0.5, 0.4],
    [0.7, 0.7, 0.04]
]))

//Oceano Infinito
sceneModels.push(new floor(200, -0.2, 200, [0, 0, 0.9], "sea", true, [
    [0.1, 0.18725, 0.1745],
    [0.396, 0.74151, 0.69102],
    [0.297254, 0.30829, 0.306678]
]))
sceneModels[3].sx = sceneModels[3].sz = 10;


// Pás

// Esquerda

sceneModels.push( new simplePaddlesModel() );

sceneModels[4].tx = -1.5; sceneModels[4].ty = 0.5; sceneModels[4].tz = -2.3;

sceneModels[4].sx = sceneModels[4].sy = sceneModels[4].sz = 2;

sceneModels[4].rotYYOn = true;

sceneModels[4].rotYYSpeed = 7;

// Direita

sceneModels.push( new simplePaddlesModel() );

sceneModels[5].tx = 1.5; sceneModels[5].ty = 0.5; sceneModels[5].tz = -2.4;

sceneModels[5].sx = sceneModels[5].sy = sceneModels[5].sz = 2;

sceneModels[5].rotYYOn = true;

sceneModels[5].rotYYSpeed = 7;

// sceneModels.push(new tree());
// sceneModels[6].ty += 4;
// sceneModels[6].sy = 4;
// sceneModels[6].sz = 0.25;
// sceneModels[6].sx = 0.25;

// Gerar edificions random tambêm
for (var i = sceneModels.length; i < 30; i++) {
    sceneModels.push(new simpleCubeModel());

    sceneModels[i].sx = sceneModels[i].sz = Math.random() * 2 + 1;
    sceneModels[i].sy = Math.random() * 12;

    sceneModels[i].rotAngleYY = Math.random() * Math.PI * 2;

    sceneModels[i].tx = Math.random() * -50;
    sceneModels[i].ty = sceneModels[i].sy;
    sceneModels[i].tz = Math.random() * -50;

    if (sceneModels[i].tz < 5) {
        sceneModels[i].tz += 2;
    }
}

// var len = sceneModels.length
//
// Árvores 
// for (var i = sceneModels.length; i < len+25; i++) {
//     if (i % 2 == 0) {
//         sceneModels.push(new tree());
//         sceneModels.push(new sphereModel(2));
//         sceneModels[i].sx = sceneModels[i].sz = 0.25;
//         sceneModels[i].sy = 3.7;
//         sceneModels[i+1].sx = sceneModels[i + 1].sz = 0.25;
//         sceneModels[i+1].sy = 3.7;
//         SceneModels[i].rotAngleYY = Math.random() * Math.PI * 2;
//         tx = Math.random() * 50;
//         tz = Math.random() * 50;
//         sceneModels[i].tx = tx;
//         sceneModels[i].ty += sceneModels[i].sy;
//         sceneModels[i].tz = tz;
//         sceneModels[i+1].tx = tx;
//         sceneModels[i+1].ty += sceneModels[i + 1].sy;
//         sceneModels[i+1].tz = tz;
//     }
// }
