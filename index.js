//////////////////////////////////////////////////////////////////////////////
//
//  WebGL_example_20.js
//
//  Animating models with global and local transformations.
//
//  References: www.learningwebgl.com + E. Angel examples
//
//  J. Madeira - October 2015
//
//////////////////////////////////////////////////////////////////////////////


//----------------------------------------------------------------------------
//
// Global Variables
//

var gl = null; // WebGL context
var shaderProgram = null;

var triangleVertexPositionBuffer = null;
var triangleVertexNormalBuffer = null;
var triangleVertexColorBuffer = null;
// var cubeVertexTextureCoordBuffer;

// The GLOBAL transformation parameters
var globalAngleYY = 0.0;
var globalTz = 0.0;

// The local transformation parameters
// The translation vector
var tx = 0.0;
var ty = 0.0;
var tz = 0.0;

// The rotation angles in degrees
var angleXX = 0.0;
var angleYY = 0.0;
var angleZZ = 0.0;

// The scaling factors
var sx = 0.5;
var sy = 0.5;
var sz = 0.5;

// NEW - GLOBAL Animation controls
var globalRotationYY_ON = 0;
var globalRotationYY_DIR = 1;
var globalRotationYY_SPEED = 1;

// NEW - Local Animation controls
var rotationXX_ON = 0;
var rotationXX_DIR = 1;
var rotationXX_SPEED = 1;

var rotationYY_ON = 0;
var rotationYY_DIR = 1;
var rotationYY_SPEED = 1;

var rotationZZ_ON = 0;
var rotationZZ_DIR = 1;
var rotationZZ_SPEED = 1;

// To allow choosing the way of drawing the model triangles
var primitiveType = null;

// To allow choosing the projection type
var projectionType = 1;

var colors = [];
var vertices = [];

var pos_Viewer = [0.0, 0.0, 0.0, 1.0];

//----------------------------------------------------------------------------
//
// The WebGL code
//

//----------------------------------------------------------------------------
//
//  Rendering
//

// Handling the Vertex and the Color Buffers

function initBuffers(model) {

	// Coordinates
	triangleVertexPositionBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, triangleVertexPositionBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(model.vertices), gl.STATIC_DRAW);
	triangleVertexPositionBuffer.itemSize = 3;
	triangleVertexPositionBuffer.numItems = model.vertices.length / 3;

	// Associating to the vertex shader
	gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute,
		triangleVertexPositionBuffer.itemSize,
		gl.FLOAT, false, 0, 0);

	// Colors
	triangleVertexColorBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, triangleVertexColorBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(model.colors), gl.STATIC_DRAW);
	triangleVertexColorBuffer.itemSize = 3;
	triangleVertexColorBuffer.numItems = colors.length / 3;

	// Associating to the vertex shader
	gl.vertexAttribPointer(shaderProgram.vertexColorAttribute,
		triangleVertexColorBuffer.itemSize,
		gl.FLOAT, false, 0, 0);

	// Vertex Normal Vectors

	triangleVertexNormalBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, triangleVertexNormalBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(model.normals), gl.STATIC_DRAW);
	triangleVertexNormalBuffer.itemSize = 3;
	triangleVertexNormalBuffer.numItems = model.normals.length / 3;

	// Associating to the vertex shader

	gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute,
		triangleVertexNormalBuffer.itemSize,
		gl.FLOAT, false, 0, 0);
}

//----------------------------------------------------------------------------

//  Drawing the model

function drawModel(model,
	mvMatrix,
	primitiveType) {

	// The the global model transformation is an input

	// Concatenate with the particular model transformations

	// Pay attention to transformation order !!

	if(model.map == 1){
		mvMatrix = mult(mvMatrix, rotationZZMatrix(model.rotAngleZZ));

		mvMatrix = mult(mvMatrix, rotationYYMatrix(model.rotAngleYY));

		mvMatrix = mult(mvMatrix, rotationXXMatrix(model.rotAngleXX));
	}

	mvMatrix = mult(mvMatrix, translationMatrix(model.tx, model.ty, model.tz));

	if(model.map == 0){
		mvMatrix = mult(mvMatrix, rotationZZMatrix(model.rotAngleZZ));

		mvMatrix = mult(mvMatrix, rotationYYMatrix(model.rotAngleYY));

		mvMatrix = mult(mvMatrix, rotationXXMatrix(model.rotAngleXX));
	}

	mvMatrix = mult(mvMatrix, scalingMatrix(model.sx, model.sy, model.sz));

	// Passing the Model View Matrix to apply the current transformation

	var mvUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");

	gl.uniformMatrix4fv(mvUniform, false, new Float32Array(flatten(mvMatrix)));

	initBuffers(model);

	// Material properties

	gl.uniform3fv(gl.getUniformLocation(shaderProgram, "k_ambient"),
		flatten(model.kAmbi));

	gl.uniform3fv(gl.getUniformLocation(shaderProgram, "k_diffuse"),
		flatten(model.kDiff));

	gl.uniform3fv(gl.getUniformLocation(shaderProgram, "k_specular"),
		flatten(model.kSpec));

	gl.uniform1f(gl.getUniformLocation(shaderProgram, "shininess"),
		model.nPhong);

	// Light Sources

	var numLights = lightSources.length;

	gl.uniform1i(gl.getUniformLocation(shaderProgram, "numLights"),
		numLights);

	// Light Sources

	for (var i = 0; i < lightSources.length; i++) {
		gl.uniform1i(gl.getUniformLocation(shaderProgram, "allLights[" + String(i) + "].isOn"),
			lightSources[i].isOn);

		gl.uniform4fv(gl.getUniformLocation(shaderProgram, "allLights[" + String(i) + "].position"),
			flatten(lightSources[i].getPosition()));

		gl.uniform3fv(gl.getUniformLocation(shaderProgram, "allLights[" + String(i) + "].intensities"),
			flatten(lightSources[i].getIntensity()));
	}

	// Drawing

	// primitiveType allows drawing as filled triangles / wireframe / vertices

	if (primitiveType == gl.LINE_LOOP) {

		// To simulate wireframe drawing!

		// No faces are defined! There are no hidden lines!

		// Taking the vertices 3 by 3 and drawing a LINE_LOOP

		var i;

		for (i = 0; i < triangleVertexPositionBuffer.numItems / 3; i++) {

			gl.drawArrays(primitiveType, 3 * i, 3);
		}
	} else {

		gl.drawArrays(primitiveType, 0, triangleVertexPositionBuffer.numItems);

	}
}

//----------------------------------------------------------------------------

//  Drawing the 3D scene

function drawScene() {

	var pMatrix;

	var mvMatrix = mat4();

	// Clearing the frame-buffer and the depth-buffer
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.clearColor(0.4, 0.9, 1, 0.5);

	// Computing the Projection Matrix

	// if (projectionType == 0) {

	// 	// For now, the default orthogonal view volume
	// 	pMatrix = ortho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);

	// 	// Global transformation !!
	// 	globalTz = 0;
	// }
	// else {
	// }

	pMatrix = perspective(45, 1, 0.05, 200);

	// Global transformation !!
	globalTz = 0;

	// Passing the Projection Matrix to apply the current projection
	var pUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");

	gl.uniformMatrix4fv(pUniform, false, new Float32Array(flatten(pMatrix)));

	gl.uniform4fv(gl.getUniformLocation(shaderProgram, "viewerPosition"),
		flatten(pos_Viewer));

	// GLOBAL TRANSFORMATION FOR THE WHOLE SCENE
	mvMatrix = translationMatrix(0, 0, globalTz);

	// NEW - Updating the position of the light sources, if required

	// FOR EACH LIGHT SOURCE

	for (var i = 0; i < lightSources.length; i++) {

		// Animating the light source, if defined
		var lightSourceMatrix = mat4();

		if (!lightSources[i].isOff()) {

			// COMPLETE THE CODE FOR THE OTHER ROTATION AXES

			if (lightSources[i].isRotYYOn()) {
				lightSourceMatrix = mult(
					lightSourceMatrix,
					rotationYYMatrix(lightSources[i].getRotAngleYY()));
			}
		}

		// NEW Passing the Light Souree Matrix to apply
		var lsmUniform = gl.getUniformLocation(shaderProgram, "allLights[" + String(i) + "].lightSourceMatrix");

		gl.uniformMatrix4fv(lsmUniform, false, new Float32Array(flatten(lightSourceMatrix)));
	}

	for (var i = 0; i < sceneModels.length; i++) {
		drawModel(sceneModels[i],
			mvMatrix,
			primitiveType);

	}
}

//----------------------------------------------------------------------------
//
//  NEW --- Animation
//

// Animation --- Updating transformation parameters


var lastTime = 0;

function animate() {

	var timeNow = new Date().getTime();

	if (lastTime != 0) {

		var elapsed = timeNow - lastTime;

		// Global rotation

		if (globalRotationYY_ON) {

			globalAngleYY += globalRotationYY_DIR * globalRotationYY_SPEED * (90 * elapsed) / 1000.0;
		}

		for (var i = 0; i < sceneModels.length; i++) {
			if (sceneModels[i].rotXXOn) {

				sceneModels[i].rotAngleXX += sceneModels[i].rotXXDir * sceneModels[i].rotXXSpeed * (90 * elapsed) / 1000.0;
			}

			if (sceneModels[i].rotYYOn) {

				sceneModels[i].rotAngleYY += sceneModels[i].rotYYDir * sceneModels[i].rotYYSpeed * (90 * elapsed) / 1000.0;
			}

			if (sceneModels[i].rotZZOn) {

				sceneModels[i].rotAngleZZ += sceneModels[i].rotZZDir * sceneModels[i].rotZZSpeed * (90 * elapsed) / 1000.0;
			}
		}

		// Rotating the light sources

		for (var i = 0; i < lightSources.length; i++) {
			if (lightSources[i].isRotYYOn()) {

				var angle = lightSources[i].getRotAngleYY() + lightSources[i].getRotationSpeed() * (90 * elapsed) / 1000.0;

				lightSources[i].setRotAngleYY(angle);
			}
		}
	}

	lastTime = timeNow;
}

// ---------- Movement

var currentlyPressedKeys = {};
var cX = 0.5;
var cY = 0.1;
var cZ = 0.5;
var cR = 1;
var lastKeyPressed = false;
var keyPressed = false;

function handleKeys(){
	lastKeyPressed = keyPressed;
	keyPressed = false;


	if(currentlyPressedKeys[87]){
		// W
		moveMap(0,0,1,0)
		keyPressed = true;
	}
	if(currentlyPressedKeys[83]){
		// S
		moveMap(0,0,-1,0)
		keyPressed = true;
	}
	if(currentlyPressedKeys[65]){
		// A
		moveMap(1,0,0,0)
		keyPressed = true;
	}
	if(currentlyPressedKeys[68]){
		// D
		moveMap(-1,0,0,0);
		keyPressed = true;
	}
	if(currentlyPressedKeys[32]){
		// Space
		moveMap(0,-1,0,0)
		keyPressed = true;
	}
	if(currentlyPressedKeys[67]){
		// C
		moveMap(0,1,0,0)
		keyPressed = true;
	}
	if(currentlyPressedKeys[81]){
		// Q
		moveMap(0,0,0,-1);
		keyPressed = true;
	}
	if(currentlyPressedKeys[69]){
		// E
		moveMap(0,0,0,1);
		keyPressed = true;
	}
	if(lastKeyPressed && !keyPressed){
		//console.log("Diminuir volume e Speed");
		sceneModels[2].rotYYSpeed = sceneModels[1].rotYYSpeed = 7;
	}else if(!lastKeyPressed && keyPressed){
		//console.log("Aumentar volume e Speed");
		sceneModels[2].rotYYSpeed = sceneModels[1].rotYYSpeed = 20;
	}

}

function moveMap(mx, my, mz, ry){
	//console.log(sceneModels[2].ty);
	let tempX = 0;
	let tempY = 0;
	let tempZ = 0;
	//Verification
	if(mx != 0){
		tempX = sceneModels[0].tx + mx * cX * Math.cos(radians(sceneModels[0].rotAngleYY));
		tempZ = sceneModels[0].tz + mx * cX * Math.sin(radians(sceneModels[0].rotAngleYY));
	}
	if(mz != 0){
		tempX = sceneModels[0].tx + mz * cZ * Math.cos(radians(sceneModels[0].rotAngleYY + 90));
		tempZ = sceneModels[0].tz + mz * cZ * Math.sin(radians(sceneModels[0].rotAngleYY + 90));
	}
	tempY = sceneModels[0].ty + my * cY;

	var tempR = sceneModels[0].rotAngleYY + ry * cR;
	var allow = nextPositionAllowed(tempX, tempY, tempZ);


	for (var i = 0; i < sceneModels.length; i++) {
		if(allow){
			if(sceneModels[i].map == 1){
				if(mx != 0){ //Strafe
					sceneModels[i].tx += mx * cX * Math.cos(radians(sceneModels[i].rotAngleYY));
					sceneModels[i].tz += mx * cX * Math.sin(radians(sceneModels[i].rotAngleYY));
				}
				if(my != 0){ //Cima/Baixo
					sceneModels[i].ty += my * cY;
				}
				if(mz != 0){ //Frente/Trás
					sceneModels[i].tx += mz * cZ * Math.cos(radians(sceneModels[i].rotAngleYY + 90));
					sceneModels[i].tz += mz * cZ * Math.sin(radians(sceneModels[i].rotAngleYY + 90));
				}
			}
		}
		if(ry != 0){ //Rodar (permite sempre)
			sceneModels[i].rotAngleYY += ry * cR;
		}
	}
	for (var j = 0; j < lightSources.length; j++) {
		//console.log(lightSources[j].position)
		if(allow){
			if(mx != 0){ //Strafe
				lightSources[j].setX(mx * cX * Math.cos(radians(lightSources[j].rotAngleYY)));
				lightSources[j].setZ(mx * cX * Math.sin(radians(lightSources[j].rotAngleYY)));
			}
			if(my != 0){ //Cima/Baixo
				lightSources[j].setY(my * cY);
			}
			if(mz != 0){ //Frente/Trás
				lightSources[j].setX(mz * cZ * Math.cos(radians(lightSources[j].rotAngleYY + 90)));
				lightSources[j].setZ(mz * cZ * Math.sin(radians(lightSources[j].rotAngleYY + 90)));
			}
		}
		if(ry != 0){ //Rodar (permite sempre)
			var angle = lightSources[j].getRotAngleYY() + (ry * cR);
			lightSources[j].setRotAngleYY(angle);
		}
	}
}


// Insert all the collision constraints
function nextPositionAllowed(i, j, k){

	//Map Collision Detection
	//X
	var c = Math.sqrt((10*j + 0.01 * k ** 2 - 100)/ -0.01);
	if (c < i) return false;
	//Y (Z is infered from the other 2 dimensions)
	c = (((-0.01 * i**2) - (0.01 * k**2) + 100) / 10);
	if(-c < j) return false;

	//Y constrain of minimum and maximum
	if(j > -0.2) return false;
	if(j < -30) return false;

	//Circular constraint (x,z minimum and maximum)
	let limit_radius = 150;
	if(i**2 + k**2 > limit_radius**2) return false;


	//console.log("Position: " + sceneModels[6].tx + ", "+ sceneModels[6].ty + ", " + sceneModels[6].tz);
	//console.log("Scale of the building: " + sceneModels[6].sy);
	// console.log("Eu: " + i + ", "+ j + ", " + k);
	// console.log(sceneModels[6].sy + " , " + sceneModels[6].ty);
	// console.log("Limite: " + (j - sceneModels[6].sy - sceneModels[6].ty));
	//Building/Tree Collision Detection
	for (var g = 0; g < sceneModels.length; g++) {
		if(sceneModels[g].building == 1){
			var x_min = -sceneModels[g].sx;
			var x_max = sceneModels[g].sx;
			var z_min = -sceneModels[g].sz;
			var z_max = sceneModels[g].sz;
			var y_max = j -sceneModels[g].sy - sceneModels[g].ty;
			//Y min is the floor (can't go below a building) //TODO Y NOT WORKING
			if((sceneModels[g].tx > x_min && sceneModels[g].tx < x_max) && (sceneModels[g].tz > z_min && sceneModels[g].tz < z_max) && j > y_max){
				//console.log("Mayday");
				gameover();
				return false; //Finish the game with drone broken call
			}
		}
	}

	return true;
}

function gameover(){
	document.getElementById("go").style.visibility = "visible";
}

//----------------------------------------------------------------------------

// Timer

function tick() {

	requestAnimFrame(tick);

	waveAnimation();

	handleKeys();

	drawScene();

	animate();
}

// WAVE Animation
var wave_radius = 150;
let ocean_weight = -0.1;
let wave_weight = 0.1;

function waveAnimation(){
	//Waves
	wave_radius -= 0.3;
	for(var i = 0; i < sceneModels[0].vertices.length; i += 3){
		var x = sceneModels[0].vertices[i];
		var y = sceneModels[0].vertices[i+1];
		var z = sceneModels[0].vertices[i+2];
		if((x**2 + z**2 > (wave_radius - 2)**2) && (x**2 + z**2 < (wave_radius + 2)**2)){
			//console.log("Subiu " + sceneModels[0].vertices[i] + "," + sceneModels[0].vertices[i + 2]);
			sceneModels[0].vertices[i+1] = wave_weight;
		}else{
			sceneModels[0].vertices[i+1] = ocean_weight;
		}
	}
	//computeVertexNormals(sceneModels[0].vertices, sceneModels[0].normals); //PERFORMANCE SUFFERS TOO MUCH FROM THIS
	if(wave_radius < 60){
		wave_radius = 150;
	}
}


//----------------------------------------------------------------------------
//
//  User Interaction
//

function outputInfos() {

}

//----------------------------------------------------------------------------

function setEventListeners() {

	function handleKeyDown(event){
		currentlyPressedKeys[event.keyCode] = true;
	}
	function handleKeyUp(event){
		currentlyPressedKeys[event.keyCode] = false;
	}

	document.onkeydown = handleKeyDown;
	document.onkeyup = handleKeyUp;

	// document.getElementById("reset-button").onclick = function () {

	// 	// The initial values
	// 	model.tx = 0.0; ty = 0.0; tz = 0.0;
	// 	angleXX = 0.0; angleYY = 0.0; angleZZ = 0.0;
	// 	sx = 0.5; sy = 0.5; sz = 0.5;
	// };
}

//----------------------------------------------------------------------------
//
// WebGL Initialization
//

function initWebGL(canvas) {
	try {

		// Create the WebGL context

		// Some browsers still need "experimental-webgl"

		gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");

		// DEFAULT: The viewport occupies the whole canvas

		// DEFAULT: The viewport background color is WHITE

		// NEW - Drawing the triangles defining the model

		primitiveType = gl.TRIANGLES;

		// DEFAULT: Face culling is DISABLED

		// Enable FACE CULLING

		gl.enable(gl.DEPTH_TEST);

		// DEFAULT: The BACK FACE is culled!!

		// The next instruction is not needed...

		gl.cullFace(gl.BACK);

		//Initial Position
		setInitialPosition(0,-20,0)


	} catch (e) {
	}
	if (!gl) {
		alert("Could not initialise WebGL, sorry! :-(");
	}
}

function setInitialPosition(x,y,z){
	for (var i = 0; i < sceneModels.length; i++) {
		if(sceneModels[i].map == 1){
			sceneModels[i].ty += x
			sceneModels[i].ty += y
			sceneModels[i].ty += z
		}
	}
}

//----------------------------------------------------------------------------

function runWebGL() {

	var canvas = document.getElementById("my-canvas");

	initWebGL(canvas);

	shaderProgram = initShaders(gl);

	setEventListeners();

	// initBuffers(model);

	tick();		// NEW --- A timer controls the rendering / animation

	outputInfos();
}
